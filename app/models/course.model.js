// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const courseSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    // Một course có nhiều review
    reviews: [{
        type: mongoose.Types.ObjectId,
        ref: "Review"
    }]
    // Trường hợp 1 course có 1 reivew 
    // reviews: {
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }
}, {
    timestamps: true
})

module.exports = mongoose.model("Course", courseSchema);